export default class ItemModel {
  constructor(id, itemName) {
    this.id = id;
    this.itemName = itemName;
    this.isCompleted = false;
  }
}
