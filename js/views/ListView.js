import ListController from "../controllers/ListController.js";

export default class ListView {
  constructor() {
    this.listController = new ListController();
    this.todoForm = document.getElementById("todoForm");
    this.todoItem = document.getElementById("todoItem");
    this.todoList = document.getElementById("todoList");
    this.sendForm();
    this.listItems();
  }

  generateItem(item) {
    return `<article class="d-flex align-items-center pb-2 border-bottom border-3" id="item-${
      item.id
    }">
    <div class="col-6">${item.itemName}</div>
    <div class="col-6 d-flex flex-wrap">
      ${
        !item.isCompleted
          ? `<button class="btn border border-1 shadow m-1 completeBtn">Complete</button>
      <button class="btn border border-1 shadow m-1 editBtn">Edit</button>`
          : ""
      }
      <button class="btn border border-1 shadow m-1 deleteBtn">Delete</button>
    </div>
  </article>`;
  }

  listItems() {
    this.todoList.innerHTML = "";
    const list = this.listController.getItems();
    for (const item of list) {
      this.todoList.innerHTML += this.generateItem(item);
    }

    const completeBtns = document.getElementsByClassName("completeBtn");
    const editBtns = document.getElementsByClassName("editBtn");
    const deleteBtns = document.getElementsByClassName("deleteBtn");

    for (const completeBtn of completeBtns) {
      completeBtn.addEventListener("click", () => {
        const id = completeBtn.parentElement.parentElement.id;
        const idNumber = id.split("-")[1];
        this.listController.setToCOmplete(Number(idNumber));
        this.listItems();
      });
    }

    for (const editBtn of editBtns) {
      editBtn.addEventListener("click", () => {
        const id = editBtn.parentElement.parentElement.id;
        const idNumber = id.split("-")[1];
        const value = this.listController.getItemValue(Number(idNumber));
        const newValue = prompt(`Changing ${value} to:`, value);
        this.listController.updateItem(Number(idNumber), newValue);
        this.listItems();
      });
    }

    for (const deleteBtn of deleteBtns) {
      deleteBtn.addEventListener("click", () => {
        const id = deleteBtn.parentElement.parentElement.id;
        const idNumber = id.split("-")[1];
        this.listController.deleteItem(Number(idNumber));
        this.listItems();
      });
    }
  }

  sendForm() {
    this.todoForm.addEventListener("submit", (e) => {
      e.preventDefault();
      this.listController.addItem(this.todoItem.value);
      this.todoItem.value = "";
      this.listItems();
    });
  }
}
