import ListView from "./views/ListView.js";

class App {
  constructor() {
    this.routes = {
      "": [],
      index: [ListView],
    };

    // instantiate the views mapped in the routes object
    this.#instantiateViews();
  }

  #instantiateViews() {
    const path = window.location.pathname;
    const file = path.substr(path.lastIndexOf("/") + 1);
    const route = file.split(".")[0];
    const views = this.#getViews(route);
    for (const view of views) {
      new view();
    }
  }

  #getViews(route) {
    return typeof this.routes[route] === "undefined" ? [] : this.routes[route];
  }
}

new App();
