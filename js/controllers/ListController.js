import ItemModel from "../models/ItemModel.js";

export default class ListController {
  constructor() {
    this.list = localStorage.getItem("gtd")
      ? JSON.parse(localStorage.getItem("gtd"))
      : [];
  }

  getItems() {
    return this.list;
  }

  getItemValue(id) {
    return this.list[id].itemName;
  }

  addItem(todoItem) {
    this.list.push(new ItemModel(this.list.length, todoItem));
    localStorage.setItem("gtd", JSON.stringify(this.list));
  }

  setToCOmplete(itemId) {
    const newList = this.list.map((item) =>
      item.id === itemId ? { ...item, isCompleted: true } : item
    );
    this.list = newList;
    localStorage.setItem("gtd", JSON.stringify(this.list));
  }

  updateItem(itemId, value) {
    const newList = this.list.map((item) =>
      item.id === itemId ? { ...item, itemName: value } : item
    );
    this.list = newList;
    localStorage.setItem("gtd", JSON.stringify(this.list));
  }

  deleteItem(itemId) {
    const newList = this.list.filter((item) => item.id !== itemId);
    this.list = newList;
    localStorage.setItem("gtd", JSON.stringify(this.list));
  }
}
